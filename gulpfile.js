'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    less = require('gulp-less'),
    concat = require('gulp-concat'),
    cssmin = require('gulp-clean-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    browserSync = require("browser-sync"),
    plumber = require('gulp-plumber'),
    reload = browserSync.reload,
    rimraf = require('rimraf'),
    fileinclude = require('gulp-file-include'),
    menu_items = require('./_project_node_modules/_menu_items/_menu_items'); //Include some generated data for template variables

var path = {
    build: {
        html: 'build/',
        js: 'build/js/',
        template_js: 'build/pages/',
        jquery: 'build/js/jquery/',
        css: 'build/css/',
        template_css: 'build/pages/',
        grid: 'build/css/grid',
        img: 'build/img/',
        fonts: 'build/fonts/',
        keyframes: 'build/'
    },
    src: {
        html: ['src/**/[^_]*.html'],
        js: ['src/js/*.js'],
        template_js: ['src/pages/**/*.js'],
        jquery: ['src/js/jquery/*.*'],
        style: 'src/style/partials/**/[^_]*.less',
        other_styles: 'src/style/[^_]*.less',
        template_styles: 'src/pages/**/[^_]*.less',
        keyframes: 'src/**/_keyframes.less',
        common_keyframes: 'src/style/_common_keyframes.less',
        grid: 'src/style/grid/**/*.less',
        img: 'src/img/**/*.[^svg|gif]*',
        svg: 'src/img/**/*.svg',
        gif: 'src/img/**/*.gif',
        fonts: 'src/fonts/**/*.*',
        php: 'src/**/*.php'
    },
    watch: {
        html: 'src/**/*.html',
        js: 'src/js/**/*.js',
        template_js: 'src/pages/**/*.js',
        style: 'src/**/*.less',
        keyframes: 'src/**/keyframes.css',
        common_keyframes: 'src/**/_common_keyframes.less',
        img: 'src/img/**/*.*',
        fonts: 'src/fonts/**/*.*',
        mixins: 'src/**/[_mixin_]*.less'
    },
    clean: './build'
};

var config = {
    server: {
        baseDir: "./build"
    }
};

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(fileinclude({
            prefix: '//=',
            basepath: '@file',
            context: {
                menu_services: new menu_items.BuildServices(), //Use module-generated data
                menu_common: new menu_items.BuildCommon() //Use module-generated data
            }
        }))
        // .pipe(rigger())
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({stream:true}))
});
gulp.task('php:build', function () {
    gulp.src(path.src.php).pipe(gulp.dest('build/'));
});
gulp.task('js:build', function () {
    gulp.src(path.src.js)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.js))
        .pipe(plumber.stop())
        .pipe(reload({stream:true}))
});
gulp.task('template_js:build', function () {
    gulp.src(path.src.template_js)
        .pipe(plumber())
        .pipe(uglify())
        .pipe(gulp.dest(path.build.template_js))
        .pipe(plumber.stop())
        .pipe(reload({stream:true}))
});
gulp.task('jquery:build', function () {
    gulp.src(path.src.jquery)
        .pipe(gulp.dest(path.build.jquery))
});
gulp.task('style:build', function () {
    gulp.src(path.src.style)
        .pipe(plumber())
        .pipe(concat('common.less'))
        .pipe(less())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({stream:true}));
    gulp.src(path.src.template_styles)
        // .pipe(rigger())
        .pipe(fileinclude({
            prefix: '//=',
            basepath: '@file'
        }))
        .pipe(less())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.template_css))
        .pipe(plumber.stop())
        .pipe(reload({stream:true}));
    gulp.src(path.src.other_styles)
    // .pipe(rigger())
        .pipe(less())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.css))
        .pipe(plumber.stop())
        .pipe(reload({stream:true}))
});
gulp.task('grid:build', function () {
    gulp.src(path.src.grid)
        .pipe(plumber())
        .pipe(concat('grid_full.less'))
        .pipe(less())
        .pipe(prefixer())
        .pipe(cssmin())
        .pipe(gulp.dest(path.build.grid))
        .pipe(plumber.stop())
        .pipe(reload({stream:true}))
});
gulp.task('keyframes:build', function () {
    gulp.src(path.src.keyframes)
        .pipe(fileinclude({
            prefix: '//=',
            basepath: '@file'
        }))
        .pipe(plumber())
        .pipe(less())
        .pipe(prefixer())
        .pipe(gulp.dest(path.build.keyframes))
        .pipe(plumber.stop())
});
gulp.task('image:build', function () {
    gulp.src(path.src.img)
        .pipe(plumber())
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(gulp.dest(path.build.img))
    gulp.src(path.src.svg)
        .pipe(gulp.dest(path.build.img))
        // .pipe(plumber())
        // .pipe(plumber.stop())
    gulp.src(path.src.gif)
        .pipe(gulp.dest(path.build.img))
    // .pipe(plumber())
    // .pipe(plumber.stop())
});

gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(plumber())
        .pipe(gulp.dest(path.build.fonts));
    gulp.src('src/style/fonts.css')
        .pipe(gulp.dest(path.build.css))
        .pipe(plumber.stop())
});

gulp.task('common_keyframes:build', function() {
    gulp.src(path.src.common_keyframes)
        .pipe(plumber())
        .pipe(fileinclude({
            prefix: '//=',
            basepath: '@file'
        }))
        .pipe(less())
        .pipe(prefixer())
        .pipe(gulp.dest(path.build.css))
        .pipe(plumber.stop());
});

gulp.task('watch', function(){
    browserSync(config);
    watch([path.watch.html], function(event, cb) {
        gulp.start('html:build');
        browserSync.reload;
    });
    watch([path.watch.style], function(event, cb) {
        gulp.start('style:build');
        browserSync.reload;
    });
    watch([path.watch.js], function(event, cb) {
        gulp.start('js:build');
        browserSync.reload;
    });
    watch([path.watch.template_js], function(event, cb) {
        gulp.start('template_js:build');
        browserSync.reload;
    });
    watch([path.watch.img], function(event, cb) {
        gulp.start('image:build');
        browserSync.reload;
    });
    watch([path.watch.fonts], function(event, cb) {
        gulp.start('fonts:build');
        browserSync.reload;
    });
    watch([path.watch.keyframes], function(event, cb) {
        gulp.start('keyframes:build');
        browserSync.reload;
    });
    watch([path.watch.common_keyframes], function(event, cb) {
        gulp.start('common_keyframes:build');
        browserSync.reload;
    });
    watch([path.watch.mixins], function(event, cb) {
        gulp.start('style:build');
        browserSync.reload;
    });
});

gulp.task('build', [
    'html:build',
    'js:build',
    'js:build',
    'jquery:build',
    'style:build',
    'grid:build',
    'image:build',
    'fonts:build',
    'keyframes:build',
    'common_keyframes:build',
    'template_js:build',
    'php:build'
]);

gulp.task('clean', function (cb) {
    rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'watch']);

